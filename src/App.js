import React from 'react';
import './App.css';
import {
    ApolloClient, HttpLink, ApolloLink, ApolloProvider,
    InMemoryCache,
    Observable,
} from '@apollo/client';
import { Helmet } from 'react-helmet';
import { SnackbarProvider } from 'notistack';

import { setContext } from 'apollo-link-context';
import { onError } from '@apollo/client/link/error';
import config from './config.json';
import MainPage from './components/MainPage';

const errorLink = onError(({
    graphQLErrors, networkError, operation,
}) => {
    if (graphQLErrors) {
        graphQLErrors.forEach(({ message, extensions }) => {
            if (extensions.code !== 'FORBIDDEN') {
                window.enqueueSnackbar(
                    `${message}`, {
                        variant: 'error',
                    },
                );
            }
            if (extensions.code === 'INVALID_TOKEN' && window.localStorage.getItem('token')) {
                window.localStorage.removeItem('token');
                window.location.reload();
            }
            return Observable.of(operation);
        });
    }
    if (networkError && networkError.statusCode !== 400) {
        window.enqueueSnackbar(`[Network error]: ${networkError}`, {
            variant: 'error',
        });
        return Observable.of(operation);
    }
    return null;
});

const link = new HttpLink({
    uri: config.uri || config.server_url,
});

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('token');

    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
        },
    };
});

const client = new ApolloClient({
    defaultOptions: {
        watchQuery: {
            fetchPolicy: 'network-only',
            errorPolicy: 'all',
        },
        query: {
            fetchPolicy: 'network-only',
            errorPolicy: 'all',
        },
        mutate: {
            errorPolicy: 'all',
        },
    },
    cache: new InMemoryCache(),
    link: ApolloLink.from([errorLink, authLink, link]),
});

function App() {
    return (
        <ApolloProvider client={client}>
            <SnackbarProvider maxSnack={4}>
                <Helmet>
                    <title>Ресурсоворот</title>
                </Helmet>
                <MainPage />
            </SnackbarProvider>
        </ApolloProvider>
    );
}

export default App;
