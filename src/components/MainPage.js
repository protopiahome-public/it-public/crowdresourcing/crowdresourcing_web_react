import React from 'react';
import { gql } from '@apollo/client';
import {
    BrowserRouter as Router, Route, Link,
} from 'react-router-dom';
import { withApollo } from '@apollo/client/react/hoc';
import { withSnackbar } from 'notistack';
// or you can use `import gql from 'graphql-tag';` instead
import Projects from './Projects';
import Register from './Register';
import Project from './Project';
import ProjectEdit from './ProjectEdit';
import User from './User';
import Profile from './Profile';
import Help from './Help';
import config from '../config.json';

class MainPage extends React.Component {
    render() {
        if (!this.state.loaded) {
            return <div></div>;
        }
        let userinfo = {};
        let user;
        const content = <div className="col-md-8">
            <Route path="/" exact component={Projects} />
            <Route path="/projects/:id" component={Project} />
            <Route path="/projects/add" component={Project} />
            <Route path="/project_edit/:id" component={ProjectEdit} />
            <Route path="/user/:id" component={User} />
            <Route path="/profile" render={() => <Profile refresh={this.init} />} />
            <Route path="/register" exact component={Register} />
            <Route path="/help" exact component={Help} />
        </div>;
        if (this.state.data) {
            userinfo = this.state.data.getCurrentUser;
            global.userinfo = userinfo;
            user = <div>
                {'Вы - '}
                <Link to="/profile">
                    {userinfo.name}
                    {' '}
                    {userinfo.family_name}
                </Link>
                {' '}
                (
                <a onClick={this.logout}>Выйти</a>
                )
            </div>;
        } else {
            global.userinfo = userinfo;
            user = <div>
                <form onSubmit={this.login}>
                    <p>
Email:
                        <input name="email" value={this.state.login.email} onChange={this.change} />
                    </p>
                    <p>
Пароль:
                        <input name="password" value={this.state.login.password} type="password" onChange={this.change} />
                    </p>
                    <p><input type="submit" /></p>
                </form>
                <div><Link to="/register">Регистрация</Link></div>
            </div>;
        }
        return (

            <div className="">
                <header></header>
                <div className="container">
                    <div className="row">
                        <Router>
                            <div className="col-md-4">
                                <h1><Link to="/">Ресурсоворот</Link></h1>
                                {user}
                                <p><Link to="/help">Помощь</Link></p>
                            </div>
                            {content}
                        </Router>
                    </div>
                </div>
                <footer></footer>
            </div>

        );
    }

    constructor(props) {
        super(props);
        this.state = { login: { email: '', password: '' }, loaded: false };
        this.change = this.change.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.init = this.init.bind(this);
    }

    componentDidMount() {
        window.enqueueSnackbar = this.props.enqueueSnackbar;
        this.init();
    }

    change(e) {
        this.state.login[e.target.name] = e.target.value;
        this.setState(this.state);
    }

    async init() {
        try {
            if (!localStorage.getItem('token')) {
                const result = await this.props.client.mutate({
                    mutation: gql`
        mutation token($input: TokenInput) {
          token(input: $input) {
            access_token
            token_type
            expires_in
            refresh_token
            __typename
          }
        }
        `,
                    variables: {
                        input: {
                            assertion: config.assertion || config.assertion_token,
                            grant_type: 'jwt-bearer',
                        },
                    },
                });
                localStorage.setItem('token', result.data.token.access_token);
            }
            const userInfo = await this.props.client.query({
                query: gql`
        query {
            getCurrentUser {
                _id
                name
                family_name
                roles
            }
        }
    `,
            });
            this.setState({ data: userInfo.data });
        } catch (e) {
            // Ignore errors
        }
        this.setState({ loaded: true });
    }

    logout() {
        localStorage.removeItem('token');
        this.setState({ data: null });
        this.init();
    }

    async login(e) {
        e.preventDefault();
        let result;
        result = await this.props.client.mutate({
            mutation: gql`
        mutation authorize($input: AuthorizeInput) {
          authorize(input: $input) {
            auth_req_id
            __typename
          }
        }
        `,
            variables: {
                input: {
                    assertion: config.assertion || config.assertion_token,
                    login_hint: this.state.login.email,
                    scope: [''],
                    user_code: this.state.login.password,
                },
            },
        });

        if (!result.data.authorize) {
            return;
        }

        result = await this.props.client.mutate({
            mutation: gql`
        mutation token($input: TokenInput) {
          token(input: $input) {
            access_token
            token_type
            expires_in
            refresh_token
            __typename
          }
        }
    `,
            variables: {
                input: {
                    assertion: config.assertion || config.assertion_token,
                    auth_req_id: result.data.authorize.auth_req_id,
                    grant_type: 'ciba',
                },
            },
        });
        localStorage.setItem('token', result.data.token.access_token);
        this.props.enqueueSnackbar('Вы вошли!', {
            variant: 'success',
        });
        this.init();
    }
}

export default withSnackbar(withApollo(MainPage));
