import React from 'react';
import { gql } from '@apollo/client';
import {
    Link,
} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
// or you can use `import gql from 'graphql-tag';` instead

import { withApollo } from '@apollo/client/react/hoc';

import ConfirmDialog from './ConfirmDialog';

class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            deleteOpen: '',
        };
        this.delete = this.delete.bind(this);
        this.refresh = this.refresh.bind(this);
    }

    delete(id) {
        this.props.client.mutate({
            mutation: gql`
            mutation($_id:ID!) {
                deleteProject(_id:$_id) {
                    _id
            }}
        `,
            variables: { _id: id },
        }).then(
            () => this.refresh(),
        );
    }

    componentDidMount() {
        this.refresh();
    }

    refresh() {
        this.props.client.query({
            query: gql`
            query {
                getProjects {
                    _id
                    title
                    owner {_id name}
                }
            }
        `,
        }).then(result => { this.setState({ data: result.data }); });
    }

    render() {
        if (!this.state.data) {
            return null;
        }
        const projects = [];
        const th = this;
        this.state.data.getProjects.forEach(i => {
            const manage = global.userinfo._id && global.userinfo._id === i.owner._id
                ? <span>
                    <Link to={`/project_edit/${i._id}`}>
                        <Tooltip title="Редактировать">
                            <IconButton size="small">
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                    </Link>
                    <Tooltip title="Удалить">
                        <IconButton project-id={i._id} onClick={() => th.setState({ deleteOpen: i._id })} size="small">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </span>
                : null;
            projects.push(
                <h2 key={i._id}>
                    <li>
                        <Link to={`/projects/${i._id}`}>{i.title}</Link>
                        {' '}
                        {manage}
                    </li>
                </h2>,
            );
        });

        const result = <>
            <div>
                <p>
                    <i>
                Ресурсоворот &mdash; это как краудфандинг, только без денег. Создавайте проекты, запрашивайте ресурсы на них, помогайте ресурсами другим проектам!
                    </i>
                </p>
                <p>
                    <i>
                Это Open Source проект.
                        {' '}
                        <a href="https://gitlab.com/protopiahome-public/it-public/crowdresourcing">Присоединяйтесь</a>
                        {' '}
к разработке!
                    </i>
                </p>
                <h2>Проекты</h2>
                {global.userinfo._id
                    ? <h4 key="new"><Link to="/project_edit/add">Добавить проект</Link></h4>
                    : 'Зарегистрируйтесь или войдите, чтобы создать проект'}
                <ul>{projects}</ul>
            </div>
            <ConfirmDialog
                open={!!this.state.deleteOpen.length}
                title="Удалить?"
                onClose={() => this.setState({ deleteOpen: '' })}
                onAction={() => this.delete(this.state.deleteOpen)}
            />
        </>;
        return result;
    }
}

export default withApollo(Projects);
