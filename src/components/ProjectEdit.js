import React from 'react';
import { gql } from '@apollo/client';
import { withRouter } from 'react-router';
import { withApollo } from '@apollo/client/react/hoc';
import { Helmet } from 'react-helmet';
// or you can use `import gql from 'graphql-tag';` instead

class ProjectEdit extends React.Component {
    project = null;

    componentDidMount() {
        if (this.props.match.params.id === 'add') {
            this.setState({ data: { getProject: { title: '' } } });
            return;
        }

        this.props.client.query({
            query: gql`
                query ($id:ID!) {
                    getProject(id:$id) {
                        _id title description add_date requests {
                                _id title count add_date request_answers {
                                        _id title count is_approved add_date owner {
                                            _id name family_name
                                        }
                                }
                            }
                            owner {
                                _id name family_name
                            }
                    }
                }
        `,
            variables: { id: this.props.match.params.id },
        }).then(result => this.setState({ data: result.data }));
    }

    render() {
        if (!this.state.data) {
            return null;
        }

        this.project = this.state.data.getProject;

        const result = (
            <div>
                <h2>{this.props.match.params.id === 'add' ? 'Добавление проекта' : 'Редактирование проекта'}</h2>
                <div>
Название:
                    <input value={this.project.title} name="title" onChange={this.handleInputChange} />
                </div>
                <div>
Описание:
                    <textarea name="description" onChange={this.handleInputChange} value={this.project.description} />
                </div>
                <div><input type="submit" onClick={this.submit} /></div>
            </div>
        );

        return <>
            <Helmet>
                <title>
                    {this.props.match.params.id === 'add' ? 'Добавление проекта' : 'Редактирование проекта'}
                </title>
            </Helmet>
            {result}
        </>;
    }

    submit() {
        this.props.client.mutate({
            mutation: gql`
                mutation($_id:ID $input: ProjectInput) {
                    changeProject(_id:$_id input:$input) {
                        _id title
                }}
        `,
            variables: { _id: this.project._id, input: { title: this.project.title, description: this.project.description } },
        }).then(
            () => { this.props.history.push('/'); },
        );
    }

    constructor(props) {
        super(props);
        this.state = {};

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submit = this.submit.bind(this);
    }

    handleInputChange(event) {
        this.project[event.target.name] = event.target.value;
        this.setState(this.state);
    }
}

export default withRouter(withApollo(ProjectEdit));
