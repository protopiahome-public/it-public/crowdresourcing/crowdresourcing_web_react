import React from 'react';
import { gql } from '@apollo/client';
import moment from 'moment';
import {
    Link,
} from 'react-router-dom';
import { withApollo } from '@apollo/client/react/hoc';
import { Helmet } from 'react-helmet';
// or you can use `import gql from 'graphql-tag';` instead
import Request from './Request.js';

class Project extends React.Component {
    constructor(props) {
        super(props);
        this.state = { project: {} };
        this.refresh = this.refresh.bind(this);
    }

    componentDidMount() {
        this.refresh(this.props.match.params.id);
    }

    refresh(id) {
        this.props.client.query({
            query: gql`
            query($id:ID!) {
                getProject(id:$id) {
                    _id title description add_date requests {
                            _id title count add_date request_answers {
                                    _id title count is_approved approve_date add_date owner {
                                        _id name family_name
                                    }
                            }
                        }
                        owner {
                            _id name family_name
                        }
                }
            }
        `,
            variables: {
                id,
            },
        }).then(result => this.setState({ project: { data: result.data } }));
    }

    render() {
        if (!this.state.project.data) {
            return null;
        }
        const project = this.state.project.data.getProject;

        let requests = [];
        if (global.userinfo._id && global.userinfo._id === project.owner._id) {
            requests = [<Request key="new" project_id={project._id} refresh={this.refresh} />];
        }
        project.requests.forEach(request => {
            requests.push(
                <Request key={request._id} state={request} project={project} project_id={project._id} refresh={this.refresh} />,
            );
        });

        let descriptionHtml = null;

        if (project.description) {
            descriptionHtml = project.description.split('\n').map((item, key) => <span key={key}>
                {item}
                <br />
            </span>);
        }

        const result = (
            <div className="">
                <div className="col-md-6">
                    <h2>{project.title}</h2>
                    <div><i>{project.add_date ? moment(project.add_date).format('DD.MM.YYYY HH:mm:ss') : null}</i></div>
                    <div>
                        <i>
                            <Link to={`/user/${project.owner._id}`}>
                                {project.owner.name}
                                {' '}
                                {project.owner.family_name}
                            </Link>
                        </i>
                    </div>
                    <p>{descriptionHtml}</p>
                </div>
                <div className="col-md-6">
                    <h2>Запросы</h2>
                    {requests}
                </div>
            </div>
        );

        return <>
            <Helmet>
                <title>
                    {project.title}
                    {' - Ресурсоворот'}
                </title>
            </Helmet>
            {result}
        </>;
    }
}

export default withApollo(Project);
