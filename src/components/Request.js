import React from 'react';
import { gql } from '@apollo/client';
import { withApollo } from '@apollo/client/react/hoc';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';

// or you can use `import gql from 'graphql-tag';` instead

import RequestAnswer from './RequestAnswer';
import ConfirmDialog from './ConfirmDialog';

class Request extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            count: '',
            deleteOpen: false,
        };

        this.toggle = this.toggle.bind(this);
        this.submit = this.submit.bind(this);
        this.delete = this.delete.bind(this);
        this.change = this.change.bind(this);
    }

    componentDidMount() {
        if (this.props.state) {
            this.setState(this.props.state);
        }
    }

    componentDidUpdate(prevProps) {
        if (JSON.stringify(prevProps.state) !== JSON.stringify(this.props.state)) {
            this.setState(this.props.state);
        }
    }

    change(e) {
        this.state[e.target.name] = e.target.value;
        this.setState(this.state);
    }

    delete() {
        this.props.client.mutate({
            mutation: gql`
                mutation($_id:ID!) {
                    deleteRequest(_id:$_id) {
                        _id title count
                }}
        `,
            variables: {
                _id: this.state._id,
            },
        }).then(
            () => this.props.refresh(this.props.project_id),
        );
    }

    submit(e) {
        this.props.client.mutate({
            mutation: gql`
                mutation($_id:ID $project_id:ID $input: RequestInput) {
                    changeRequest(_id:$_id project_id:$project_id input:$input) {
                        _id title count
                }}
        `,
            variables: { _id: this.state._id, project_id: this.props.project_id, input: { title: this.state.title, count: parseInt(this.state.count) } },
        }).then(() => this.props.refresh(this.props.project_id));
        this.toggle();
        e.preventDefault();
    }

    toggle() {
        this.state.type = this.state.type === 'edit' ? 'view' : 'edit';
        this.setState(this.state);
    }

    render() {
        if (!this.state) {
            return null;
        }

        let result;

        if (this.state.type === 'edit') {
            result = (
                <div style={{ border: 'solid 1px black', padding: '10px', margin: '10px' }}>
                    <form onSubmit={this.submit}>
                        <div>
Название:
                            <input onChange={this.change} value={this.state.title} name="title" />
                        </div>
                        <div>
Количество:
                            <input onChange={this.change} value={this.state.count} name="count" />
                        </div>
                        <div>
                            <input type="submit" />
                            {' '}
                            <input
                                type="button"
                                value="Отменить"
                                onClick={
                                    () => this.setState({ type: 'view', ...this.props.state })
                                }
                            />
                        </div>
                    </form>
                </div>
            );
        } else if (this.state._id) {
            const answers = [];
            const th = this;
            this.state.request_answers.forEach(answer => {
                answers.push(
                    <RequestAnswer key={answer._id} state={answer} request_id={th.state._id} project={th.props.project} project_id={th.props.project_id} refresh={th.props.refresh} />,
                );
            });
            if (global.userinfo._id) {
                answers.push(
                    <RequestAnswer key="new" request_id={this.state._id} project_id={this.props.project_id} project={this.props.project} refresh={this.props.refresh} />,
                );
            } else {
                answers.push('Зарегистрируйтесь или войдите, чтобы ответить на запрос');
            }
            const manage = (global.userinfo._id && this.props.project.owner._id === global.userinfo._id)
                ? <span>
                    <Tooltip title="Редактировать">
                        <IconButton onClick={this.toggle} size="small">
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Удалить">
                        <IconButton onClick={() => this.setState({ deleteOpen: true })} size="small">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </span>
                : null;

            result = (
                <div style={{ border: 'solid 1px black', padding: '10px', margin: '10px' }}>
                    <h3>
                        {this.state.title}
                        {' '}
                        {manage}
                    </h3>
                    <div>
                        {this.state.count}
                        {' '}
шт.
                    </div>
                    <h4>Ответы</h4>
                    {answers}
                </div>
            );
        } else {
            result = (
                <h3><a onClick={this.toggle}>Добавить</a></h3>
            );
        }

        return <>
            {result}
            <ConfirmDialog
                open={this.state.deleteOpen}
                title="Удалить?"
                onClose={() => this.setState({ deleteOpen: false })}
                onAction={() => this.delete()}
            />
        </>;
    }
}

export default withApollo(Request);
