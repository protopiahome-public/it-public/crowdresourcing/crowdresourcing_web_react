import React from 'react';
import { gql } from '@apollo/client';
import { withRouter } from 'react-router';
import { withApollo } from '@apollo/client/react/hoc';
import { Helmet } from 'react-helmet';
import { withSnackbar } from 'notistack';
// or you can use `import gql from 'graphql-tag';` instead

class Register extends React.Component {
    project = null;

    render() {
        const result = (
            <form onSubmit={this.submit}>
                <h2>Регистрация</h2>
                <div>
Имя:
                    <input value={this.state.name} name="name" onChange={this.change} />
                </div>
                <div>
Email:
                    <input value={this.state.email} name="email" onChange={this.change} />
                </div>
                <div>
Пароль:
                    <input value={this.state.password} type="password" name="password" onChange={this.change} />
                </div>
                <div>
                    <input type="submit" />
                </div>
            </form>
        );

        return <>
            <Helmet>
                <title>
                   Регистрация
                </title>
            </Helmet>
            {result}
        </>;
    }

    submit(e) {
        e.preventDefault();

        this.props.client.mutate({
            mutation: gql`
            mutation($input: UserInput) {
                registerUser(input:$input) {
                    _id
            }}
`,
            variables: { input: { name: this.state.name, email: this.state.email, password: this.state.password } },
        }).then(result => {
            if (!result.errors) {
                this.props.enqueueSnackbar('Вы зарегистрированы! Теперь войдите', {
                    variant: 'success',
                });
                this.props.history.push('/');
            }
        });
    }

    constructor(props) {
        super(props);
        this.state = {};

        this.change = this.change.bind(this);
        this.submit = this.submit.bind(this);
    }

    change(e) {
        this.state[e.target.name] = e.target.value;
        this.setState(this.state);
    }
}

export default withSnackbar(withRouter(withApollo(Register)));
