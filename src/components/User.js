import React from 'react';
import { gql } from '@apollo/client';
import { withApollo } from '@apollo/client/react/hoc';
import { Helmet } from 'react-helmet';

// or you can use `import gql from 'graphql-tag';` instead

class User extends React.Component {
    project = null;

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.client.query({
            query: gql`
                query ($id:ID!) {
                    getUser(id:$id) {
                        _id name family_name contacts
                }}
        `,
            variables: {
                id: this.props.match.params.id,
            },
        }).then(result => this.setState({ data: result.data }));
    }

    render() {
        if (!this.state.data) {
            return null;
        }
        const user = this.state.data.getUser;
        let contactsHtml = null;
        if (user.contacts) {
            contactsHtml = user.contacts.split('\n').map((item, key) => <span key={key}>
                {item}
                <br />
            </span>);
        }
        return <>
            <Helmet>
                <title>
                    {`Профиль пользователя: ${user.name} ${user.family_name}`}
                </title>
            </Helmet>
            <div>
                <h2>
                    {`Профиль пользователя: ${user.name} ${user.family_name}`}
                </h2>
                <div>{contactsHtml}</div>
            </div>
        </>;
    }
}

export default withApollo(User);
