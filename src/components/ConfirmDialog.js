import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

const ConfirmDialog = props => <Dialog
    open={props.open}
    onClose={props.onClose}
>
    <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
    <DialogActions>
        <Button
            onClick={() => {
                props.onAction();
                props.onClose();
            }}
            color="primary"
            autoFocus
        >
        Да
        </Button>
        <Button onClick={props.onClose} color="secondary">
        Нет
        </Button>
    </DialogActions>
</Dialog>;

export default ConfirmDialog;
