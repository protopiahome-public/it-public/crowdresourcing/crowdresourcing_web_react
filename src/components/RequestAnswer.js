import React from 'react';
import { gql } from '@apollo/client';
import moment from 'moment';
import {
    Link,
} from 'react-router-dom';
import { withApollo } from '@apollo/client/react/hoc';
import { withSnackbar } from 'notistack';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
// or you can use `import gql from 'graphql-tag';` instead

import ConfirmDialog from './ConfirmDialog';

class RequestAnswer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            count: '',
            deleteOpen: false,
        };

        this.toggle = this.toggle.bind(this);
        this.submit = this.submit.bind(this);
        this.delete = this.delete.bind(this);
        this.change = this.change.bind(this);
        this.approve = this.approve.bind(this);
        this.unapprove = this.unapprove.bind(this);
    }

    componentDidMount() {
        if (this.props.state) {
            this.setState(this.props.state);
        }
    }

    componentDidUpdate(prevProps) {
        if (JSON.stringify(prevProps.state) !== JSON.stringify(this.props.state)) {
            this.setState(this.props.state);
        }
    }

    change(e) {
        this.state[e.target.name] = e.target.value;
        this.setState(this.state);
    }

    delete() {
        this.props.client.mutate({
            mutation: gql`
                mutation($_id:ID!) {
                    deleteRequestAnswer(_id:$_id) {
                        _id count
                }}
        `,
            variables: {
                _id: this.state._id,
            },
        }).then(
            () => this.props.refresh(this.props.project_id),
        );
    }

    submit(e) {
        this.props.client.mutate({
            mutation: gql`
                mutation($_id:ID $request_id:ID $input: RequestInput) {
                    changeRequestAnswer(_id:$_id request_id:$request_id input:$input) {
                        _id count
                }}
        `,
            variables: { _id: this.state._id, request_id: this.props.request_id, input: { title: this.state.title, count: parseInt(this.state.count) } },
        }).then(
            () => this.props.refresh(this.props.project_id),
        );
        this.toggle();
        e.preventDefault();
    }

    unapprove() {
        this.props.client.mutate({
            mutation: gql`
                mutation($request_answer_id:ID) {
                    unapproveRequestAnswer(request_answer_id:$request_answer_id) {
                        _id count
                }}
        `,
            variables: { request_answer_id: this.state._id },
        }).then(() => {
            this.props.refresh(this.props.project_id);
            this.props.enqueueSnackbar('Вы опровергли получение ресурса', {
                variant: 'error',
            });
        });
    }

    approve() {
        this.props.client.mutate({
            mutation: gql`
                mutation($request_answer_id:ID) {
                    approveRequestAnswer(request_answer_id:$request_answer_id) {
                        _id count
                }}
        `,
            variables: { request_answer_id: this.state._id },
        }).then(() => {
            this.props.refresh(this.props.project_id);
            this.props.enqueueSnackbar('Вы подтвердили получение ресурса', {
                variant: 'success',
            });
        });
    }

    toggle() {
        this.state.type = this.state.type === 'edit' ? 'view' : 'edit';
        this.setState(this.state);
    }

    render() {
        if (!this.state) {
            return null;
        }

        if (this.state.mutation) {
            this.props.refresh(this.props.project_id);
            this.state.mutation = null;
        }

        let result;

        if (this.state.type === 'edit') {
            result = (
                <div style={{ border: 'solid 1px black', padding: '10px', margin: '10px' }}>
                    <form onSubmit={this.submit}>
                        <div>
Количество:
                            <input onChange={this.change} value={this.state.count} name="count" />
                        </div>
                        <div>
                            <input type="submit" />
                            {' '}
                            <input
                                type="button"
                                value="Отменить"
                                onClick={
                                    () => this.setState({ type: 'view', ...this.props.state })
                                }
                            />
                        </div>
                    </form>
                </div>
            );
        } else if (this.state._id) {
            const manage = (global.userinfo._id && global.userinfo._id === this.state.owner._id)
                ? <span>
                    <Tooltip title="Редактировать">
                        <IconButton onClick={this.toggle} size="small">
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Удалить">
                        <IconButton onClick={() => this.setState({ deleteOpen: true })} size="small">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </span> : null;
            const manageShow = global.userinfo._id === this.props.project.owner._id ? 'initial' : 'none';
            const approved = this.state.is_approved
                ? <>
                    <div>
                        {'Подтверждено в '}
                        <i>{this.state.approve_date ? moment(this.state.approve_date).format('DD.MM.YYYY HH:mm:ss') : null}</i>
                    </div>
                    <div>
                        <a style={{ display: manageShow }} onClick={this.unapprove}>Отменить</a>
                    </div>
                </>
                : <div>
                    {'Не подтверждено. '}
                    <a style={{ display: manageShow }} onClick={this.approve}>Подтвердить</a>
                </div>;
            result = (
                <div style={{ border: 'solid 1px black', padding: '10px', margin: '10px' }}>
                    <div>
                        <Link to={`/user/${this.state.owner._id}`}>
                            {this.state.owner.name}
                            {' '}
                            {this.state.owner.family_name}
                        </Link>
                    </div>
                    {' '}
                    {manage}
                    <div><i>{this.state.add_date ? moment(this.state.add_date).format('DD.MM.YYYY HH:mm:ss') : null}</i></div>
                    <div>
                        {this.state.count}
                        {' '}
шт.
                    </div>
                    {approved}
                </div>
            );
        } else {
            result = (
                <div><a onClick={this.toggle}>Добавить</a></div>
            );
        }

        return <>
            {result}
            <ConfirmDialog
                open={this.state.deleteOpen}
                title="Удалить?"
                onClose={() => this.setState({ deleteOpen: false })}
                onAction={() => this.delete()}
            />
        </>;
    }
}

export default withSnackbar(withApollo(RequestAnswer));
