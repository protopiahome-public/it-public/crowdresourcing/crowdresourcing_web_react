import React from 'react';
import { gql } from '@apollo/client';
import { withRouter } from 'react-router';
import { withApollo } from '@apollo/client/react/hoc';
import { Helmet } from 'react-helmet';
// or you can use `import gql from 'graphql-tag';` instead

class Profile extends React.Component {
    user = null;

    componentDidMount() {
        this.props.client.query({
            query: gql`
                query {
                    getCurrentUser {
                        _id name family_name email contacts
                    }
                }
        `,
        }).then(result => this.setState({ data: result.data }));
    }

    render() {
        if (!this.state.data) {
            return null;
        }

        this.user = this.state.data.getCurrentUser;

        const result = (
            <div>
                <h2>Редактирование профиля</h2>
                <table>
                    <tbody>
                        <tr>
                            <td>Имя:</td>
                            <td><input value={this.user.name} name="name" onChange={this.handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Фамилия:</td>
                            <td><input value={this.user.family_name} name="family_name" onChange={this.handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><input value={this.user.email} name="email" onChange={this.handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Контакты:</td>
                            <td>
                                {' '}
                                <textarea name="contacts" onChange={this.handleInputChange} value={this.user.contacts} />
                            </td>
                        </tr>
                        <tr>
                            <td>Пароль:</td>
                            <td><input value={this.user.password} name="password" type="password" onChange={this.handleInputChange} /></td>
                        </tr>
                        <tr><td><input type="submit" onClick={this.submit} /></td></tr>
                    </tbody>
                </table>
            </div>
        );

        return <>
            <Helmet>
                <title>
                    Мой профиль
                </title>
            </Helmet>
            {result}
        </>;
    }

    submit() {
        this.props.client.mutate({
            mutation: gql`
                mutation($input: UserCurrentInput) {
                    changeCurrentUser(input:$input) {
                        _id name family_name email contacts
                }}
        `,
            variables: {
                input: {
                    name: this.user.name, family_name: this.user.family_name, email: this.user.email, password: this.user.password, contacts: this.user.contacts,
                },
            },
        }).then(
            () => {
                this.props.refresh();
                this.props.history.push('/');
            },
        );
    }

    constructor(props) {
        super(props);

        this.state = {};
        this.handleInputChange = this.handleInputChange.bind(this);
        this.submit = this.submit.bind(this);
    }

    handleInputChange(event) {
        this.user[event.target.name] = event.target.value;
        this.setState(this.state);
    }
}

export default withRouter(withApollo(Profile));
